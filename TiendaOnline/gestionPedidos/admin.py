from django.contrib import admin

from gestionPedidos.models import Users, Articule, Pedidos
# Register your models here.

class UsersAdmin(admin.ModelAdmin):
     list_display = ("name", "address","phone")
     search_fields =("name", "phone")

class ArticuleAdmin(admin.ModelAdmin):
     list_filter = ("section",)

class PedidosAdmin(admin.ModelAdmin):
     list_display = ("number", "date")
     list_filter = ("date",)
     date_hierarchy = ("date")

admin.site.register(Users, UsersAdmin)
admin.site.register(Articule, ArticuleAdmin)
admin.site.register(Pedidos, PedidosAdmin)