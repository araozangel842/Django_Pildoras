from django.shortcuts import render
from django.http import HttpResponse
from gestionPedidos.models import Articule
from django.conf import settings

# Importar para email
from django.core.mail import send_mail

# Importar el form.py para las validaciones
from gestionPedidos.form import LoginForm

# Create your views here.
def busqueda(request):
     return render(request, 'busqueda.html')

def buscar(request):

     if request.GET['product']:
               # mensaje="Artículo buscado: %s" % request.GET["product"]
               productoDB = request.GET["product"]
               if len(productoDB)>20:
                    mensaje="Texto demasiado largo"
               else:
                    articule=Articule.objects.filter(section__icontains=productoDB)

                    return render(request, 'product.html',{"articule":articule, "query":productoDB})
     else:
          mensaje="Introduce texto"

     return HttpResponse(mensaje)

def contacto(request):
     if request.method == "POST":
          miForm = LoginForm(request.POST)

          if miForm.is_valid():
               information = miForm.cleaned_data

               send_mail(information['asunto'], information['mensaje'], information.get('mensaje',''), ['araozangel842@gmail.com'],)
          return render(request,"gracias.html")
     else:
          miForm = LoginForm()#se devuelve el form vacio
     return render(request, "form_contact.html",{"form": miForm})

          # Funcion de envio de email
     #      subject = request.POST["asunto"]
     #      # mensaje = request.POST["email"]
     #      message = request.POST["mensaje"] + " " + request.POST["email"]
     #      email_from = settings.EMAIL_HOST_USER

     #      recipient_list = ["araozangel842@gmail.com"]
     #      send_mail(subject, message, email_from, recipient_list)

     #      return render(request, "gracias.html")
     # return render(request,"contacto.html")