from django.db import models

# Create your models here.

class Users(models.Model):
     name = models.CharField(max_length=30)
     address = models.CharField(max_length=50, verbose_name='The address is:')
     email = models.EmailField()    
     phone = models.CharField(max_length=10)

     def __str__(self):
          return f"{self.name}"

class Articule(models.Model):
     name = models.CharField(max_length=30)
     section = models.CharField(max_length=20)
     price = models.IntegerField()

     def __str__(self):
          return f"El {self.name} de la  {self.section} vale {self.price}"

class Pedidos(models.Model):
     number = models.IntegerField()
     date = models.DateField()
     entregado = models.BooleanField()