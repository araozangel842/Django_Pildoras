from django import forms

class LoginForm(forms.Form):
     asunto = forms.CharField()
     email = forms.EmailField()
     mensaje = forms.CharField()